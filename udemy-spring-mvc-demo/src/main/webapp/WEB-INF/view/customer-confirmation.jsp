<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Customer Confirmation</title>
</head>
<body>
	The Customer is confirmed: ${customer.firstName} ${customer.lastName}
	<br><br>
	Customer FreePasses : ${customer.freePasses}
	<br><br>
	Customer PostalCode : ${customer.postalCode}
	<br><br>
	Customer CourseCode : ${customer.courseCode}
	<br><br>
</body>
</html>