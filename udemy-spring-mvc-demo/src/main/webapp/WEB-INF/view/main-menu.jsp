<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Home Page</title>
<link rel="stylesheet" type="text/css" 
		  href="${pageContext.request.contextPath}/resources/css/my-test.css">

    <script src="${pageContext.request.contextPath}/resources/js/simple-test.js"></script>
</head>
<body>
<h2>Spring MVC Demo - Home Page</h2>

<a href="hello/showForm">Hello world form</a>

<br><br>

<a href="student/showForm">Student form</a>
<br><br>

<a href="customer/showForm">Customer form</a>
<br><br>
<img src="${pageContext.request.contextPath}/resources/images/spring-logo.png" />
<br><br>
<input type="button" onclick="doSomeWork()" value="Clique Ici"/>
</body>
</html>