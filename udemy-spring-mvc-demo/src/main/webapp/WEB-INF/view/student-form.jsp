<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Student Registration Form</title>
</head>
<body>
	<form:form action="processForm" modelAttribute="student">
		Prénom : <form:input path="firstName"/>
		<br><br>
		Nom : <form:input path="lastName"/>
		<br><br>
		Country : <form:select path="country"> 
	     <form:options items="${theCountryOptions}" />
	    </form:select>
		<br><br>
		Favorite Language :
			<form:radiobuttons path="favoriteLanguage" items="${student.favoriteLanguageOptions}"  />
		<br><br>
		Operating Systems :
			Linux <form:checkbox path="operatingSystems" value="Linux"/>
			Mac <form:checkbox path="operatingSystems" value="Mac"/>
			Windows <form:checkbox path="operatingSystems" value="Windows"/>
		<br><br>
		<input type="submit" value="Valider">
		<br><br>
	</form:form>
</body>
</html>