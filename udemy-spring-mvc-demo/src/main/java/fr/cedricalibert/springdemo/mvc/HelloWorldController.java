package fr.cedricalibert.springdemo.mvc;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/hello")
public class HelloWorldController {
	
	//controller method to see the initial form
	@RequestMapping("/showForm")
	public String showForm() {
		return "helloworld-form";
	}
	
	//controller method to process the html form
	@RequestMapping("/processForm")
	public String processForm() {
		return "helloworld";
	}
	
	//read form data and add to the model
	@RequestMapping("/processFormVersionTwo")
	public String processFormVersionTwo(HttpServletRequest request, Model model) {
		
		// read the request parameter from the html form
		String theName = request.getParameter("studentName");
		// converte the data to all caps
		theName = theName.toUpperCase();
		// create the message
		String result = "Yo "+theName;
		// add message to the model
		model.addAttribute("message", result);
		
		return "helloworld";
	}
	
	//read form data and add to the model
	@RequestMapping("/processFormVersionThree")
	public String processFormVersionThree(
			@RequestParam("studentName") String theName, Model model) {
		
	
		// converte the data to all caps
		theName = theName.toUpperCase();
		// create the message
		String result = "Yo v3 "+theName;
		// add message to the model
		model.addAttribute("message", result);
		
		return "helloworld";
	}
}
