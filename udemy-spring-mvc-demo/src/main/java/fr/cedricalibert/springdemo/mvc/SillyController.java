package fr.cedricalibert.springdemo.mvc;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class SillyController {
	//controller method to see the initial form
	@RequestMapping("/showForm")
	public String showForm() {
		return "helloworld-form";
	}
}
